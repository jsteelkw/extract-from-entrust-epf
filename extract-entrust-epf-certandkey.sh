#!/bin/bash

# Copyright 2020-2022 Deric Sullivan
# deric.sullivan@gmail.com

# SPDX-License-Identifier: AGPL-3.0-or-later


# 2020/10/01  ds - Original script.  Based on my notes and mini scripts from
#                  understanding the Entrust EPF file format.
# 2020/10/08  ds - Updates.  Now in a "usable" state if others want
#                  to try it.
# 2021/07/13  ds - Add code to also extract Encryption Certificate and Private
#                  key.  Before the script only extracted the Signing
#                  Certificate and Private Key.
# 2021/07/20  ds - Add more comments in code.  Rename Signing files.
# 2021/07/21  ds - Make code more general for extracting Encription Private
#                  key from EPF file.
# 2021/08/05  ds - Also extract the CA certificate.
# 2022/05/25  ds - Add copyright and license.

log_sensitive() {
    if [ -n "$LOG_SENSITIVE_DATA" ];
    then
        printf 'Treat the next line of output as a secret.\n%s\n' "$*"
    fi
}

date -u
echo "Start of script"

CHECK_EPF_CRC_CMD="check-entrust-epf-crc.py"

echo "This script will extract the following from an Entrust EPF file:"
echo "    Encryption Certificate"
echo "    Encryption Private Key"
echo "    Signing Certificate"
echo "    Signing Private Key"
echo "    CA Certificate for full chain"
echo

echo
echo "How it does this:"
echo
echo "Overview:"
echo "Your Entrust EPF file is protected by a passphrase/password."
echo "That's the one you type if you login to Entrust."
echo "This script will require that passphrase to extract your"
echo "Certificate and Key."
echo "This script will then save your Certificate in one file and"
echo "your private Key in another file.  The private Key file will"
echo "be protected by another passphrase (which could match the"
echo "EPF passphrase if you choose)."
echo
echo "Part 1:"
echo "This script will use the EPF passphrase to generate a symmetric"
echo "encryption/decription key and an Initialization Vector (IV)."
echo "Treat the generated symmetric key and IV as secret."
echo "With this generated symmetric key and IV and the EPF file,"
echo "someone can have access to your private Key."
echo "This script does not save the symmetric key and IV but some of the"
echo "debugging code may display them on the screen/output."
echo "My code for generating the symmetric key and IV is from some knowledge"
echo "of encryption standards but also a lot of trial and error to get around"
echo "the non-standard (and not publicly accessible) part of Entrust's"
echo "EPF format.  If Entrust changes their non-standard encoding of the"
echo "EPF file to different (also non-standard) encoding then there might"
echo "be a fair amount of work to keep this script functional."
echo
echo "Part 2:"
echo "With the information obtained from Part 1, data for some of the sections"
echo "in the EPF file are extracted.  This includes certificates and keys."
echo "My code for parsing the sections of the EPF file are likely very"
echo "fragile to any changes to the format of the EPF file.  I.e. I would"
echo "not be surprised if a new version of Entrust casued this script to"
echo "fail. However, it should be reasonably easy to fix any such failures"
echo "if the changes are just to the ordering of sections / sub-sections."
echo
echo "Note:"
echo "With a bit of work, you could use the symmetric key and IV (from Part 1)"
echo "to decrypt more parts of the EPF file (like in Part 2).  I just"
echo "extracted what I needed."
echo

echo

echo "Start part 1"
echo
# Get the filename that we are working with.
# Either from the command line or from user input.
if [ "x$1" = "x" ];
then
    read -p "Enter the Entrust EPF file name: " epf_filename
else
    epf_filename="${1}"
fi
echo "epf_filename = " ${epf_filename}

# This is a good place to call the python script which verifies
# the CRC value in the EPF file.
# Maybe this script should stop if the CRC does not appear to be good, but
# I chose to have it continue just in case some day the EPF format changes
# and the CRC check starts to fail but the rest might still work.
if command -v ${CHECK_EPF_CRC_CMD}
then
    echo "Found a script for verifying the CRC of the EPF file."
    echo "Running that script now."
    ${CHECK_EPF_CRC_CMD} ${epf_filename}
elif command -v ./${CHECK_EPF_CRC_CMD}
then
    echo "Found a script for verifying the CRC of the EPF file."
    echo "Running that script now."
    ./${CHECK_EPF_CRC_CMD} ${epf_filename}
else
    echo "Could not find the script to verify the CRC in the EPF file."
    echo "Skipping that verification."
fi

# Get the Salt value from the EPF file.
epf_salt=$(cat ${epf_filename} | sed -n -e '/^SaltValue=/{p;q}' | sed 's/^SaltValue=//' | tr -d '\r\n')
echo "epf_salt = " ${epf_salt}

# Get the hash count value from the EPF file.
#declare -i epf_hashcount
epf_hashcount=$(cat ${epf_filename} | sed -n -e '/^HashCount=/{p;q}' | sed 's/^HashCount=//' | tr -d '\r\n')
echo "epf_hashcount = " ${epf_hashcount}

# Get the token value from the EPF file.
epf_token=$(cat ${epf_filename} | sed -n -e '/^Token=/{p;q}' | sed 's/^Token=//' | tr -d '\r\n')
echo "epf_token = " ${epf_token}

# Get the EPF File password from the user.
read -s -p "Enter the Entrust EPF file password: " epf_password
echo
echo "continuing..."

# Using the password and the salt, generate the encryption key and
# IV (initialization vector).  Note that the salt is used in its
# base64 encoded form, which I find a little odd.
# The Password Based Encryption (PBE) Key Derivation Function (KDF)
# does not exactly match any option from RFC8018
# https://tools.ietf.org/html/rfc8018
# although there are similarities.
# Entrust uses SHA1 (and possibly MD5 in some versions) and an
# iteration count on the password and salt
# in order to generate the key and IV.
# Since SHA1 does not generate enough bytes for both the key and IV,
# (at least not for CAST5)
# the process is repeated, but this time the byte 0x01 is appended to
# the password and salt.
# The key and half of the IV come from the first pass.  The rest of
# the IV comes from the second pass.

# The following section is commented out because I switched from using
# files for temporary storage, to bash variable.  I wanted to solve two
# problems that I had with the files version: 1) performance,
# 2) security (I didn't want files with sensitive info being left behind).
# In the end, using variables instead of files did not make a big difference
# in performance but hopefully it's helping on the security side.
## pass 1 to generate the key and part of the IV
#declare -i index
#index=1
#echo -n "${epf_password}" > tmp.bin
#echo -n "${epf_salt}" >> tmp.bin
#while [ $index -lt $(($epf_hashcount + 1)) ]
#do
#    cp tmp.bin tmp2.bin
#    openssl sha1 -binary -out tmp.bin tmp2.bin
#    ((index++))
#done
#pbe_partial=$(od -A n -t x1 tmp.bin | tr -d '[:space:]')
#echo "pbe_partial = " ${pbe_partial}
# Would need to delete the tmp file here if we were to use the above code.


# File base performance vs variables:  ~ 43 seconds for 10000 hash with
# variables and ~ 46 seconds with files (on my laptop in 2020).
# On a Windows terminal server it appears to take ~ 10 seconds
# to login to Entrust, which would include both sets of hashes (2 x 10000).
# Note the use of xxd below to go back and forth between hex and binary
# is because I can't figure out a better way to deal with binary data
# in bash variables.
#date -u
# pass 1 to generate the key and part of the IV
echo "pass 1 to generate the key and part of the IV"
declare -i index
index=1
tmp_to_hash=$(echo -n "${epf_password}""${epf_salt}" | xxd -p -c 256)
echo "start time: " $(date -u)
while [ $index -lt $(($epf_hashcount + 1)) ]
do
    tmp_to_hash2="${tmp_to_hash}"
    tmp_to_hash=$(echo -n ${tmp_to_hash2} | xxd -p -r -c 256 | openssl dgst -sha1 -binary | xxd -p -c 256)
    ((index++))
done
echo "end time: " $(date -u)
pbe_partial=${tmp_to_hash}
log_sensitive "pbe_partial = ${pbe_partial}"


# At this stage we have the full Key and part of the IV.
encryption_key_in_hex=$(echo -n ${pbe_partial} | head --bytes=32)
encryption_iv_in_hex=$(echo -n ${pbe_partial} | tail --bytes=-8)

# As above, this section is commented out and replaced by a different
# method.
## pass 2 to generate the rest of the IV
#index=1
#echo -n "${epf_password}" > tmp.bin
#echo -n "${epf_salt}" >> tmp.bin
#echo -en "\x01" >> tmp.bin
#while [ $index -lt $(($epf_hashcount + 1)) ]
#do
#    cp tmp.bin tmp2.bin
#    openssl sha1 -binary -out tmp.bin tmp2.bin
#    ((index++))
#done
#pbe_partial=$(od -A n -t x1 tmp.bin | tr -d '[:space:]')
#echo "pbe_partial = " ${pbe_partial}
# Would need to delete the tmp file here if we were to use the above code.

# pass 2 to generate the rest of the IV
echo "pass 2 to generate the rest of the IV"
declare -i index
index=1
tmp_to_hash=$(echo -e -n "${epf_password}""${epf_salt}""\x01" | xxd -p -c 256)
echo "start time: " $(date -u)
while [ $index -lt $(($epf_hashcount + 1)) ]
do
    tmp_to_hash2="${tmp_to_hash}"
    tmp_to_hash=$(echo -n ${tmp_to_hash2} | xxd -p -r -c 256 | openssl dgst -sha1 -binary | xxd -p -c 256)
    ((index++))
done
echo "end time: " $(date -u)
pbe_partial=${tmp_to_hash}
log_sensitive "pbe_partial = ${pbe_partial}"


# Complete the IV.
encryption_iv_in_hex=$(echo -n ${encryption_iv_in_hex}; echo -n ${pbe_partial} | head --bytes=8)
log_sensitive "encryption_key_in_hex = ${encryption_key_in_hex}"
log_sensitive "encryption_iv_in_hex = ${encryption_iv_in_hex}"

# Check if the generated key and IV are what we expect for this EPF file.
# Encrypting 8 bytes of all zeros should produce the Token found
# in the Password Token section in the EPF file.
test_token=$(echo -ne "\x00\x00\x00\x00\x00\x00\x00\x00" | openssl enc -cast5-cbc -nosalt -K ${encryption_key_in_hex} -iv ${encryption_iv_in_hex} | head --bytes=8 | od -A n -t x1 | tr -d '[:space:]')
echo "test_token = " ${test_token^^}
if [ ${epf_token} = ${test_token^^} ]
then
    echo "The generated token matches the token from the EPF file."
    echo "The supplied password is correct."
else
    echo "The generated token does not match the token from the EPF file."
    echo "The supplied password is incorrect."
    echo "Exiting."
    exit 1
fi


echo
echo "Start part 2"

current_timestamp=$(date -u +%Y%m%d%H%M%S)

echo
echo "Extracting the Encryption Certificate."
# Setup the filename to be used for the certificate.
filename_cert="user_encrypt_"${current_timestamp}".pem"
# Extract encryption certificate from EPF file.
# There appear to be a number of extra characters at the top and bottom
# of the certificate.  These have to be removed when converting from
# DER to PEM.
# The extra characters at the top seem to be a descriptive string.
CERT_NON_STANDARD_EXTRA_CHARS="User CertificateCertificate"
save_LANG=${LANG}
save_LC_ALL=${LC_ALL}
LANG=C LC_ALL=C
bytelen_cert_extra_top=${#CERT_NON_STANDARD_EXTRA_CHARS}
# There may be some extra characters at the bottom but
# I didn't need to remove them this time (so I put the value to zero).
# I'm not sure why sometimes openssl will require the length to match
# and other times not.
bytelen_cert_extra_bottom=0
LANG=${save_LANG} LC_ALL=${save_LC_ALL}
# Start by isolating the base64 encoding of the encrypted encryption certificate.
cat ${epf_filename} | sed -E -e '1,/^\[User Certificate\]/d' | sed -E -e '/^\r$|^$/,$d' | sed -n -E -e '/^&Certificate=/,$p' | sed -E -e 's/^&Certificate=|^_continue_=//' |
# Then base64 decode the encrypted encryption certificate.
base64 -d -i |
# Then decrypt the certificate.
openssl enc -d -cast5-cbc -nosalt -K ${encryption_key_in_hex} -iv ${encryption_iv_in_hex} |
# Then remove the extra characters at the top.
tail --bytes=+$((${bytelen_cert_extra_top} + 1)) |
# Then remove the extra characters at the bottom.
head --bytes=-${bytelen_cert_extra_bottom} |
# Create the certificate in PEM format.
openssl x509 -inform DER \
> ${filename_cert}
echo "The certificate can be found in the file: " ${filename_cert}

echo
echo "Extracting the Encryption Private Key."
echo "You will be asked to provide a password (twice) to protect"
echo "the private key.  This can be a new password or it can match your EPF"
echo "file password."
# Setup the filename to be used for the key.
filename_key="user_encrypt_"${current_timestamp}".key"
# Extract encryption private key from EPF file.
# There appear to be a number of extra characters at the top and bottom
# of the key.  These have to be removed when converting from
# DER to PEM.
# The extra characters at the top seem to be a descriptive string.
# TODO: ?????Fix the following string to get the number from the KeyCount
# variable as is done with the awk a little farther down.  "...Key1" will
# work for keys 1 to 9 but once we get double digits then the string length
# will be off by one.
CERT_NON_STANDARD_EXTRA_CHARS="Private KeysKey1"
save_LANG=${LANG}
save_LC_ALL=${LC_ALL}
LANG=C LC_ALL=C
bytelen_cert_extra_top=${#CERT_NON_STANDARD_EXTRA_CHARS}
# There may be some extra characters at the bottom but
# I didn't need to remove them this time (so I put the value to zero).
# I'm not sure why sometimes openssl will require the length to match
# and other times not.
bytelen_cert_extra_bottom=0
LANG=${save_LANG} LC_ALL=${save_LC_ALL}
# Start by isolating the base64 encoding of the encrypted encryption private key.
#cat ${epf_filename} | sed -E -e '1,/^\[Private Keys\]/d;/^KeyCount=/Q' | sed -n -E -e '/^&Key5=/,$p' | sed -E -e 's/^&Key5=|^_continue_=//' |

awk '
# 2021/07/21  ds - script to pull encoded private key out of EPF file
BEGIN {inprivkey=0; lastkey=0 ; inlastkey=0 ; secondpass=0 }

#{print $0}
#{print NR, "inprivkey=",inprivkey,"lastkey=",lastkey,"inlastkey=",inlastkey,"secondpass=",secondpass}
/^\[Private Keys\]/{inprivkey=1;next}
{if (inprivkey==0) next}
#{print "Now in priv key section"}
/^KeyCount=/{if (inprivkey==1  && secondpass==0) { gsub(/^KeyCount=/, "", $0); gsub(/\r/, "", $0) ; lastkey="^&Key"$0"=" ; inprivkey=0 ; inlastkey=0 ; secondpass=1 ; nextfile } }
{if (secondpass==0) { next } }
($0~lastkey){ inlastkey=1 }
/^KeyCount=/{inlastkey=0 ; next}
/^\r$|^$/{inlastkey=0 ; inprivkey=0 ; next}
{if (inlastkey==0) next}
{if (inlastkey==1 && inprivkey==1 && secondpass==1) { regex1=lastkey"|^_continue_=" ; gsub(regex1,"", $0) } }
{if (inlastkey==1 && inprivkey==1 && secondpass==1) print $0}

#END { print "lastkey =", lastkey }
' ${epf_filename} ${epf_filename} |

# Then base64 decode the encrypted encryption private key.
base64 -d -i |
# Then decrypt the key.
openssl enc -d -cast5-cbc -nosalt -K ${encryption_key_in_hex} -iv ${encryption_iv_in_hex} |
# debug: The following "tee" will generate a file of the data immediately
#        after decription.  This could be useful to deal with the TODO
#        listed above (i.e. to fix our hard coding of Key1).
#        If you do use this "tee" to debug, don't forget to delete the file
#        it creates, once you're done with it, because that file will
#        contain a private key without password protection.
#tee ${filename_key}.afterdec |
# Then remove the extra characters at the top.
tail --bytes=+$((${bytelen_cert_extra_top} + 1)) |
# Then remove the extra characters at the bottom.
head --bytes=-${bytelen_cert_extra_bottom} |
# Create the private key output in PKCS8 PEM and protected by a passphrase.
openssl pkcs8 -topk8 -inform DER \
> ${filename_key}
echo "The key can be found in the file: " ${filename_key}

echo "If you want to create a PKCS12 file then you can run something like:"
echo "openssl pkcs12 -export -name encrypt -out user_encrypt_${current_timestamp}.p12 -inkey ${filename_key} -in ${filename_cert}"

echo
echo "Extracting the Signing Certificate."
# Setup the filename to be used for the certificate.
filename_cert="user_sign_"${current_timestamp}".pem"
# Extract signing certificate from EPF file.
# There appear to be a number of extra characters at the top and bottom
# of the certificate.  These have to be removed when converting from
# DER to PEM.
# The extra characters at the top seem to be a descriptive string.
CERT_NON_STANDARD_EXTRA_CHARS="Digital SignatureCertificate"
save_LANG=${LANG}
save_LC_ALL=${LC_ALL}
LANG=C LC_ALL=C
bytelen_cert_extra_top=${#CERT_NON_STANDARD_EXTRA_CHARS}
# I don't know what the extra characters at the bottom are, but there
# seem to be 8 of them.
bytelen_cert_extra_bottom=8
LANG=${save_LANG} LC_ALL=${save_LC_ALL}
# Start by isolating the base64 encoding of the encrypted signing certificate.
cat ${epf_filename} | sed -E -e '1,/^\[Digital Signature\]/d' | sed -E -e '/^\r$|^$/,$d' | sed -n -E -e '/^&Certificate=/,$p' | sed -E -e 's/^&Certificate=|^_continue_=//' |
# Then base64 decode the encrypted signing certificate.
base64 -d -i |
# Then decrypt the certificate.
openssl enc -d -cast5-cbc -nosalt -K ${encryption_key_in_hex} -iv ${encryption_iv_in_hex} |
# Then remove the extra characters at the top.
tail --bytes=+$((${bytelen_cert_extra_top} + 1)) |
# Then remove the extra characters at the bottom.
head --bytes=-${bytelen_cert_extra_bottom} |
# Create the certificate in PEM format.
openssl x509 -inform DER \
> ${filename_cert}
echo "The certificate can be found in the file: " ${filename_cert}

echo
echo "Extracting the Signing Private Key."
echo "You will be asked to provide a password (twice) to protect"
echo "the private key.  This can be a new password or it can match your EPF"
echo "file password."
# Setup the filename to be used for the key.
filename_key="user_sign_"${current_timestamp}".key"
# Extract signing private key from EPF file.
# There appear to be a number of extra characters at the top and bottom
# of the key.  These have to be removed when converting from
# DER to PEM.
# The extra characters at the top seem to be a descriptive string.
CERT_NON_STANDARD_EXTRA_CHARS="Digital SignatureKey"
save_LANG=${LANG}
save_LC_ALL=${LC_ALL}
LANG=C LC_ALL=C
bytelen_cert_extra_top=${#CERT_NON_STANDARD_EXTRA_CHARS}
# I don't know what the extra characters at the bottom are, but there
# seem to be 8 of them.
bytelen_cert_extra_bottom=8
LANG=${save_LANG} LC_ALL=${save_LC_ALL}
# Start by isolating the base64 encoding of the encrypted signing private key.
cat ${epf_filename} | sed -E -e '1,/^\[Digital Signature\]/d;/^&Certificate=/Q;s/^&Key=|^_continue_=//' |
# Then base64 decode the encrypted signing private key.
base64 -d -i |
# Then decrypt the key.
openssl enc -d -cast5-cbc -nosalt -K ${encryption_key_in_hex} -iv ${encryption_iv_in_hex} |
# Then remove the extra characters at the top.
tail --bytes=+$((${bytelen_cert_extra_top} + 1)) |
# Then remove the extra characters at the bottom.
head --bytes=-${bytelen_cert_extra_bottom} |
# Create the private key output in PKCS8 PEM and protected by a passphrase.
openssl pkcs8 -topk8 -inform DER \
> ${filename_key}
echo "The key can be found in the file: " ${filename_key}

echo "If you want to create a PKCS12 file then you can run something like:"
echo "openssl pkcs12 -export -name sign -out user_sign_${current_timestamp}.p12 -inkey ${filename_key} -in ${filename_cert}"

echo
echo "TODO: ????? Confirm if this section being extracted contains also"
echo "the Root or just the intermediates... Either way, what is extracted"
echo "works, but it would be nice to make sure our comments below reflect"
echo "what is actually being done."
echo "Extracting the CA Certificates (intermediates)."
# Setup the filename to be used for the certificate.
filename_cert="user_cacertintermediates_"${current_timestamp}".pem"
# Extract CA certificates (intermediates) from EPF file.
# There appear to be a number of extra characters at the top and bottom
# of the certificate.  These have to be removed when converting from
# DER to PEM.
# The extra characters at the top seem to be a descriptive string.
CERT_NON_STANDARD_EXTRA_CHARS="CA CertificatesCertificate"
save_LANG=${LANG}
save_LC_ALL=${LC_ALL}
LANG=C LC_ALL=C
bytelen_cert_extra_top=${#CERT_NON_STANDARD_EXTRA_CHARS}
# In addition, there seems to be an extra depth of ASN1 encoding to
# the CA Certificates.  By removing the next 4 bytes, which represent
# an ASN1 SEQUENCE tag and length, openssl x509 will be able to
# understand the certificate (otherwise asn1parse can still parse but
# not x509).
bytelen_cert_extra_top=$((${bytelen_cert_extra_top} + 4))
# I don't know what the extra characters at the bottom are, but there
# seem to be 8 of them.
bytelen_cert_extra_bottom=8
LANG=${save_LANG} LC_ALL=${save_LC_ALL}
# Start by isolating the base64 encoding of the encrypted CA certificates.
cat ${epf_filename} | sed -E -e '1,/^\[CA Certificates\]/d' | sed -E -e '/^\r$|^$/,$d' | sed -n -E -e '/^&Certificate=/,$p' | sed -E -e 's/^&Certificate=|^_continue_=//' |
# Then base64 decode the encrypted signing certificate.
base64 -d -i |
# Then decrypt the certificate.
openssl enc -d -cast5-cbc -nosalt -K ${encryption_key_in_hex} -iv ${encryption_iv_in_hex} |
# Then remove the extra characters at the top.
tail --bytes=+$((${bytelen_cert_extra_top} + 1)) |
# Then remove the extra characters at the bottom.
head --bytes=-${bytelen_cert_extra_bottom} |
# Create the certificate in PEM format.
openssl x509 -inform DER \
> ${filename_cert}
echo "The CA certificates (intermediate) can be found in the file: " ${filename_cert}
echo "You can use this CA certificates file to add to the Encryption or "
echo "Signing certificates.  Some applications may expect the full chain"
echo "of certificates to be in the Certificate (PEM or P12 files)."
echo "Example:"
echo "for Encryption PEM:"
echo "cat user_encrypt_${current_timestamp}.pem ${filename_cert} > user_encrypt_fullchain_${current_timestamp}.pem"
echo "for Encryption P12:"
echo "openssl pkcs12 -export -name encrypt_fullchain -out user_encrypt_fullchain_${current_timestamp}.p12 -inkey user_encrypt_${current_timestamp}.key -in user_encrypt_fullchain_${current_timestamp}.pem"
echo "for Signing PEM:"
echo "cat user_sign_${current_timestamp}.pem ${filename_cert} > user_sign_fullchain_${current_timestamp}.pem"
echo "for Signing P12:"
echo "openssl pkcs12 -export -name sign_fullchain -out user_sign_fullchain_${current_timestamp}.p12 -inkey user_sign_${current_timestamp}.key -in user_sign_fullchain_${current_timestamp}.pem"

echo
echo "End of script"
date -u

exit 0

