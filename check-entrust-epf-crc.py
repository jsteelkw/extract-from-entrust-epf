#!/usr/bin/python3
##!/usr/bin/python

# Copyright 2020-2022 Deric Sullivan
# deric.sullivan@gmail.com

# SPDX-License-Identifier: AGPL-3.0-or-later


# This script will:
#    - Verify the CRC in an Entrust EPF file.

# 2020/09/25  ds - Original script.  Based on my script which I used
#                  to figure out the CRC format.
# 2022/05/25  ds - Add copyright and license.

# The versions of Entrust which I tested use a 16 bit CRC with a seemingly
# non standard polynomial.
# Using a CRC similar to CRC-16-CCITT
# (https://en.wikipedia.org/wiki/Cyclic_redundancy_check)
# but with polynomial of 0x2021 (reversed = 0x8404).
# You can get a python
# crc generator capable of accepting that polynomial from
# http://crcmod.sourceforge.net/crcmod.html
# That python module requires that you add the often implicit high order bit
# to the polynomial (i.e. 0x12021).

# This script may still contain comments / code left over from my tests
# where I was trying to figure out the Entrust CRC format (i.e. what
# parts of the EPF file were part of the CRC and what polynomial was used
# or other non standard features were present).

import sys
import datetime
import argparse

import crcmod

import re

PROGRAM_VERSION = "20220525120000"

CRC_POLYNOMIAL = 0x12021


def main(argv=sys.argv[1:]):
    """Verify the CRC in an Entrust EPF file."""

    try:
        args = None
        parser = argparse.ArgumentParser(description=main.__doc__)
        parser.add_argument('--version', action='version', version='%(prog)s ' + PROGRAM_VERSION)
        parser.add_argument('--verbose', '-v', action='count', default=0,
                help="output more details of script execution (default: %(default)s)")
        # 2020/09/29  ds - Note that argparse.FileType does not work well
        #  for stdin.  See https://bugs.python.org/issue14156
        #  as a workaround you can specify /dev/stdin
        parser.add_argument('infile', nargs='?', type=argparse.FileType('r'),
            default=sys.stdin,
            help = """# 2020/09/29  ds - Note that argparse.FileType does not work well
        #  for stdin.  See https://bugs.python.org/issue14156
        #  as a workaround you can specify /dev/stdin
""")

        args = parser.parse_args()
        #print("args = ", args)

        if args.verbose:
            print("verbose is on")
            start_timestamp = datetime.datetime.utcnow().replace(microsecond=0)
            print("Start of script")
            print(start_timestamp.isoformat())

        # DS: I wonder if I should read this in differently.  E.g.
        # I'm taking for granted that the file read in will not be
        # large.
        epf_file_contents = args.infile.read()

        #print("testds: debug")
        #exit(1)

        thecrc=int(epf_file_contents.split("\nCRC=")[1].split("\n")[0], 16)
        if args.verbose:
            print("What we're looking for:")
            print("CRC=", hex(thecrc), " (hex), ", thecrc, " (decimal)")


        # The CRC in the Password Token section of the EPF file
        # is from performing a CRC on the values of the variables
        # listed in the Password Token section up to the CRC.
        # All values are concatenated together and end of lines are
        # stripped out.
        # Note that the Salt remains in the base64 encoded form.
        # The parsing applied here would be fragile to any changes
        # that Entrust might make to future versions of the EPF
        # format.  However, as long as any format changes keep the
        # same polynomial and simply add variables before the CRC,
        # then this parsing code should be easy to modify.
        data_tobecrced = re.sub("SaltValue=|\nToken=|\nProtection=|\nProfile Version=|\nMAC Algorithm=|\nHashCount=", "", epf_file_contents.split("[Password Token]\n",1)[1].split("\nCRC=",1)[0])

        
        acrcclass = crcmod.Crc(CRC_POLYNOMIAL)
        acrcclass.update(data_tobecrced.encode())
        acrc = acrcclass.crcValue
        
        if args.verbose:
            print("What we found:")
            print("CRC=", hex(acrc), " (hex), ", acrc, " (decimal)")

        #print("testds: debug")
        #exit(1)


        if acrc == thecrc:
            print("The CRC in this file is what we expected.")
        else:
            print("The CRC in this file is NOT what we expected.")

    finally:
        if args != None:
            if args.verbose:
                print(u"End of script")
                end_timestamp = datetime.datetime.utcnow().replace(microsecond=0)
                print(end_timestamp.isoformat())



if __name__ == "__main__":
    main(sys.argv[1:])
